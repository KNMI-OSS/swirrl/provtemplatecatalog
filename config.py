# config.py

import os
from authomatic.providers import oauth2, oauth1, openid

CONFIG = {
    
    'github': { # Your internal provider name
           
        # Provider class
        'class_': oauth2.GitHub,
	'id': 3,
	'access_headers': {'User-Agent': 'Awesome-Octocat-App'},
        # If you're not running this in a docker container, then
        # uncomment these lines, and comment the lines obtaining
        # the key and secret from the environment
        #'consumer_key': '<consumer_key>', 
        #'consumer_secret': '<consumer_secret>',
        'consumer_key': os.environ['PROV_TMPL_github_KEY'], 
        'consumer_secret': os.environ['PROV_TMPL_github_SECRET'],
	'scope': oauth2.GitHub.user_info_scope,  
    },

}

PROVSTORE = {
    'Host': '<url of  Triplestore>',
    'Authorization' : '<Base64 encoding of username and password for Triplestore>'
}

