#!/bin/bash

function usage() {
  echo "import_templates.bash -u repo-url -r revision -c template-dir"
  echo ""
  echo "-u repo-url     - URL to the git repository that contains the templates"
  echo "-r revision     - Revision of the git repository to checkout, can also be"
  echo "                  an existing branch or tag."
  echo "-c template-dir - Directory that contains the templates. This needs to include"
  echo "                  the repository root of the repository cloned from the given"
  echo "                  url. E.g. if repo-url is https://gitlab.com/KNMI-OSS/swirrl/swirrl-api"
  echo "                  then template-dir needs to be swirrl-api/prov-templates."
}

while getopts c:u:r: option ; do
  case ${option} in
    (c) CONTENT_DIR=${OPTARG}
        ;;
    (u) REPO_URL=${OPTARG}
        ;;
    (r) REVISION=${OPTARG}
        ;;
    (\?) echo "Invalid option ${OPTARG}"
        usage
        exit
        ;;
  esac
done

if [ -n "${REPO_URL}" ] ; then
  workdir=`pwd`
  git clone ${REPO_URL}
  cd ${CONTENT_DIR}
  git checkout ${REVISION}
  cd ${workdir}
fi

./import_templates.py -c ${CONTENT_DIR}
