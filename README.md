# PROV-Template Catalog

Based on https://github.com/EnvriPlus-PROV/ProvTemplateCatalog project. 
Adapted for deployment in kubernetes and docker-compose for the SWIRRL-API
project at https://gitlab.com/KNMI-OSS/swirrl/swirrl-api. Extended with
a deployment to ECS https://gitlab.com/KNMI/ewc/provtemplate-deploy.

## Setup

If you want to be able to login to the service with a browser or other http
client, you will need to setup a github OAuth application with the following URLs:

- Home page URL: http://your-hostname-here/prov-template-catalog/
- Authorization callback URL: http://your-hostname-here/prov-template-catalog/api/login/github

You can use localhost for the hostname if you are running it locally only.
When using minikube, use the result from `minikube ip` as hostname and make
sure to use HTTP unless you actually have HTTPS configured with a valid
certificate.

Set the following environment variables (applies to both docker-compose
and kubernetes deployments):
```shell
export PROV_TMPL_JWT_SECRET=`pwgen -1`
export PROV_TMPL_github_SECRET=<obtained when creating github OAuth application>
export PROV_TMPL_github_KEY=<obtained when creating github OAuth application>
```

## Running with Docker-compose

Create a directory where mongo-db can store its data:
```shell
export PROV_TMPL_DATABASE=./data/db
mkdir -p ${PROV_TMPL_DATABASE}
chmod 777 ${PROV_TMPL_DATABASE}
```

And startup the docker-compose services:
```shell
docker-compose up --build
```

To being everything back down again:
```shell
docker-compose down
```

If started locally, the application can then be accessed through
http://localhost/prov-template-catalog.
Logging in should work with your own github account if you set up OAuth.

### Backing up the database

The following command dumps the database to `TemplateData.dump` in the current
directory.
```shell
docker exec mongo-db sh -c 'exec mongodump -d TemplateData --archive' > TemplateData.dump
```

### Restoring database backup

If you want to initialise the app with data from the swirrl-api project, then
download the dump file and restore the dump with the following commands:
```shell
docker cp TemplateData.dump mongo-db:/tmp
docker exec mongo-db sh -c 'exec mongorestore --archive=/tmp/TemplateData.dump'
```
The most recent dumpfile is always a CI/CD job artifact (access to KNMI group
required), but the repo at https://gitlab.com/KNMI-OSS/swirrl/swirrl-api
has a dump in its data directory, but this one might not be up to date.

### Adding swirrl-api templates to catalog

Assuming you're deploying [swirrl-api](https://gitlab.com/KNMI-OSS/swirrl/swirrl-api)
and want to add the templates in its prov-templates directory you
can use the addcontent.py script. After spinning up the docker-compose
as above, run the following commands:

```shell
docker build -t prov-template-import -f Dockerfile.import .
docker run --rm --network provtemplatecatalog_internal_network prov-template-import \
   -u https://gitlab.com/KNMI-OSS/swirrl/swirrl-api \
   -r master -c swirrl-api/prov-templates
```

The container clones the latest [swirrl-api](https://gitlab.com/KNMI-OSS/swirrl/swirrl-api)
and inserts all *.trig files into the database. Of course you could use a different
URL. Look at the usage of the `import_templates.bash` script for directions on how
to use.

## Deploying into kubernetes

When deploying this application as part of the swirrl-api system on a
development or production environment, you can use the
[swirrl-ansible](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible)
deployment procedure. Make sure you've set all the environment
variables mentioned in the Setup section above and anything mentioned in the [swirrl-ansible
README](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible/-/blob/master/README.adoc).

The application is then reachable through
http://kubernetes-cluster-ip/prov-template-catalog.  Logging in should
work with your own github account if you set up OAuth. Make sure you
use http and not https here, or the github authentication will break.

### Restoring database backup

Download the latest dump from the scheduled job artifacts at
https://gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/-/pipelines. Unzip
the file and run the restore-db.bash script with the extracted
dumpfile as argument:

```shell
./kubernetes/restore-db.bash TemplateData.dump
```

### Importing templates from swirrl-api

This step is automatically executed when running the ansible playbook
from [swirrl-ansible](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible).
This will then also use the git commit short sha of swirrl-api and
swirrl-workflow defined in swirrl-ansible to pull the images from.

As in the docker-compose setup, we can import templates from a
directory. The commands below would do such a thing. It imports all
templates from swirrl-api before importing all the templates from
swirrl-workflow.

```shell
kubectl -n swirrl run --rm --attach --restart=Never --image-pull-policy=Always \
  --image=registry.gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/prov-template-import:latest \
  --env=PROV_TMPL_MONGODB_HOSTPORT=prov-template-catalog-mongodb \
  --pod-running-timeout=2m \
  import-templates \
  -- -u https://gitlab.com/KNMI-OSS/swirrl/swirrl-api -r master -c swirrl-api/prov-templates
kubectl -n swirrl run --rm --attach --restart=Never --image-pull-policy=Always \
  --image=registry.gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/prov-template-import:latest \
  --env=PROV_TMPL_MONGODB_HOSTPORT=prov-template-catalog-mongodb \
  --pod-running-timeout=2m \
  import-templates \
  -- -u https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow -r master -c swirrl-workflow/prov-templates

```

As needed, the `:latest` tag can be replaced by a valid hash.

## Expanding templates

No idea if these examples still work.

### Example template(*) (in provn):
```
document
  prefix xml <http://www.w3.org/XML/1998/namespace>
  prefix foaf <http://xmlns.com/foaf/0.1/>
  prefix rdfs <http://www.w3.org/2000/01/rdf-schema#>
  prefix rdf <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  prefix var <http://openprovenance.org/var#>
  prefix tmpl <http://openprovenance.org/tmpl#>
  prefix vargen <http://openprovenance.org/vargen#>

  bundle vargen:bundleId
    prefix xml <http://www.w3.org/XML/1998/namespace>
    prefix foaf <http://xmlns.com/foaf/0.1/>
    prefix rdfs <http://www.w3.org/2000/01/rdf-schema#>
    prefix rdf <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    prefix var <http://openprovenance.org/var#>
    prefix tmpl <http://openprovenance.org/tmpl#>
    prefix vargen <http://openprovenance.org/vargen#>

    wasAttributedTo(var:quote, var:author)
    entity(var:quote, [prov:value='var:value'])
    entity(var:author, [prov:type='prov:Person', foaf:name='var:name'])
  endBundle
endDocument
```

### Example bindings (in trig):
```
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix tmpl: <http://openprovenance.org/tmpl#> .
@prefix var: <http://openprovenance.org/var#> .
@prefix ex: <http://example.com/#> .

var:author a prov:Entity;
           tmpl:value_0 <http://orcid.org/0000-0002-3494-120X>.
var:name   a prov:Entity;
           tmpl:2dvalue_0_0 "Luc Moreau".
var:quote  a prov:Entity;
           tmpl:value_0 ex:quote1.
var:value  a prov:Entity;
           tmpl:2dvalue_0_0 "A Little Provenance Goes a Long Way".
```

### Python example to expand with GET
```python
import requests
import urllib

host_name = 'localhost'
template_id = '5c3dbc4d5a13e60008b76240' ## update with id obtained when adding the template
trig_string = """
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix tmpl: <http://openprovenance.org/tmpl#> .
@prefix var: <http://openprovenance.org/var#> .
@prefix ex: <http://example.com/#> .

var:author a prov:Entity;
           tmpl:value_0 <http://orcid.org/0000-0002-3494-120X>.
var:name   a prov:Entity;
           tmpl:2dvalue_0_0 "Luc Moreau".
var:quote  a prov:Entity;
           tmpl:value_0 ex:quote1.
var:value  a prov:Entity;
           tmpl:2dvalue_0_0 "A Little Provenance Goes a Long Way".
"""
url_encoded_trig_string = urllib.urlencode({"bindings": trig_string})
r = requests.get('https://' + host_name + '/templates/' + template_id +
    '/expand?fmt=provjson&writeprov=false&bindver=v2&' + url_encoded_trig_string,
    verify=False)

print r.text
```

### Python example to expand with POST
```python
import requests

host_name = 'localhost'
template_id = '5c3dbc4d5a13e60008b76240' ## update with id obtained when adding the template
trig_string = """
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix tmpl: <http://openprovenance.org/tmpl#> .
@prefix var: <http://openprovenance.org/var#> .
@prefix ex: <http://example.com/#> .

var:author a prov:Entity;
           tmpl:value_0 <http://orcid.org/0000-0002-3494-120X>.
var:name   a prov:Entity;
           tmpl:2dvalue_0_0 "Luc Moreau".
var:quote  a prov:Entity;
           tmpl:value_0 ex:quote1.
var:value  a prov:Entity;
           tmpl:2dvalue_0_0 "A Little Provenance Goes a Long Way".
"""

r = requests.post('https://' + host_name + '/templates/' + template_id +
    '/expand?fmt=provjson&writeprov=false&bindver=v2',
        data=trig_string, verify=False)

print r.text
```

(*) Example taken from Luc Moreau's ProvToolbox tutorial at
https://lucmoreau.wordpress.com/2015/07/30/provtoolbox-tutorial-4-templates-for-provenance-part-1/

[swirrl]: https://gitlab.com/KNMI-OSS/swirrl
