#!/usr/bin/env python3

import argparse, glob, os, os.path, datetime

## We need to set some environment before we import the API app,
## or we'll get exceptions.
os.environ['PROV_TMPL_github_KEY']=""
os.environ['PROV_TMPL_github_SECRET']=""
os.environ['PROV_TMPL_JWT_SECRET']=""

if not 'PROV_TMPL_MONGODB_HOSTPORT' in os.environ:
    print("Need to set PROV_TMPL_MONGODB_HOSTPORT environment variable as for API.")
    exit(1)
from app import application, addTemplateImpl, updateTemplateImpl, renderProvFileImpl, getTemplatesFromDb

## We need to set APPLICATION ROOT and SERVER_NAME so the url_for()
## calls in addTemplate render the correct URL. The SERVER_NAME is
## needed, otherwise url_for() will not work at all, but we ignore
## its value by setting _external=False
application.config.update(APPLICATION_ROOT=os.environ['SCRIPT_NAME'], SERVER_NAME="prov-template-api")

def main(content_path):
    print(content_path)
    trigfiles = glob.glob(content_path+"/*.trig")
    for trigfilename in trigfiles:
        print("Trig file: "+trigfilename)
        with open(trigfilename) as trigfile:
            trigfile_data = trigfile.read()
            trigfile_svg = renderProvFileImpl(trigfile_data, "prov-o trig")
            new_catalog_obj = create_catalog_object(trigfilename, trigfile_data, trigfile_svg.decode("utf-8"))
            catalog_obj = template_title_exists(new_catalog_obj["title"])
            if catalog_obj:
                print(catalog_obj["title"], "already exists in catalog.")
                catalog_obj["id"] = str(catalog_obj["_id"])
                catalog_obj["prov"] = new_catalog_obj["prov"]
                catalog_obj["provsvg"] = new_catalog_obj["provsvg"]
                catalog_obj["modified"] = datetime.datetime.utcnow().isoformat()
                updateTemplateImpl(catalog_obj)
                continue
            with application.app_context():
                res = addTemplateImpl(new_catalog_obj, {"userid": "swirrl-api", "siteid": "localhost"})
                print("%s result: %s" % (trigfilename, res))

def create_catalog_object(trigfilename, trigfile_data, trigfile_svg):
    now = datetime.datetime.utcnow().isoformat()
    json_obj = {
        "title": os.path.splitext(os.path.basename(trigfilename))[0],
        "subject": "SWIRRL-API",
        "description": "https://gitlab.com/KNMI-OSS/swirrl/swirrl-api",
        "type": "prov-o trig",
        "coverage": "",
        "comment": "Imported by addcontent.py",
        "creator": "addcontent.py",
        "created": now,
        "modified": now,
        "prov": trigfile_data,
        "provsvg": trigfile_svg
    }
    return json_obj

def template_title_exists(template_title):
    template_list = getTemplatesFromDb()
    for template in template_list:
        if template["title"] == template_title:
            return template
    return False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Add templates to catalog')
    parser.add_argument('-c', '--content', type=str, nargs='?', required=True,
                        help="Directory containing trig files to import.",
                        dest='content_path')

    main(parser.parse_args().content_path)