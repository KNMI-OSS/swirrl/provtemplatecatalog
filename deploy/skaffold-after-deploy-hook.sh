#!/bin/sh

# Read environment variables set by post-build hooks.
pwd
. ./provtemplatecatalog.env

kubectl -n ${NAMESPACE} run --rm --attach --restart=Never \
    --image=${PROVTEMPLATE_IMPORT_IMAGE} \
    --env=PROV_TMPL_MONGODB_HOSTPORT=prov-template-catalog-mongodb --pod-running-timeout=10m \
    --overrides="
      {\"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"import-templates\"},
        \"spec\":{
          \"volumes\":[{
            \"name\":\"prov-templates\",\"configMap\":{\"name\":\"swirrl-workflow-prov-templates\"}}],
          \"containers\":[{
            \"name\":\"import-templates\",
            \"image\":\"${PROVTEMPLATE_IMPORT_IMAGE}\",
            \"args\": [ \"-c\", \"/prov-templates\" ],
            \"env\": [{ \"name\": \"PROV_TMPL_MONGODB_HOSTPORT\", \"value\":\"prov-template-catalog-mongodb\" }],
            \"volumeMounts\":[{
              \"name\":\"prov-templates\",
              \"mountPath\":\"/prov-templates\"
            }]
          }]
        }}" \
    import-templates \
    -- -c /prov-templates

kubectl -n ${NAMESPACE} run --rm --attach --restart=Never \
    --image=${PROVTEMPLATE_IMPORT_IMAGE} \
    --env=PROV_TMPL_MONGODB_HOSTPORT=prov-template-catalog-mongodb --pod-running-timeout=10m \
    --overrides="
      {\"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"import-templates\"},
        \"spec\":{
          \"volumes\":[{
            \"name\":\"prov-templates\",\"configMap\":{\"name\":\"swirrl-api-prov-templates\"}}],
          \"containers\":[{
            \"name\":\"import-templates\",
            \"image\":\"${PROVTEMPLATE_IMPORT_IMAGE}\",
            \"args\": [ \"-c\", \"/prov-templates\" ],
            \"env\": [{ \"name\": \"PROV_TMPL_MONGODB_HOSTPORT\", \"value\":\"prov-template-catalog-mongodb\" }],
            \"volumeMounts\":[{
              \"name\":\"prov-templates\",
              \"mountPath\":\"/prov-templates\"
            }]
          }]
        }}" \
    import-templates \
    -- -c /prov-templates
