#!/bin/sh

## Create .env file for post-deploy hook.

KEY=$1
PROPERTIES=provtemplatecatalog.env

if [ ! -e ${PROPERTIES} ] ; then
  ## Use appending operator here, in case a parallel build wrote the file after this check
  echo "${KEY}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
else
  if grep -q "${KEY}" ${PROPERTIES} ; then
    sed -i -e "s^${KEY}=.*^${KEY}=${SKAFFOLD_IMAGE}^" ${PROPERTIES}
  else
    echo "${KEY}=${SKAFFOLD_IMAGE}" >> ${PROPERTIES}
  fi
fi
