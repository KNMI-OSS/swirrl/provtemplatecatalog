#!/bin/sh

cd ../swirrl-api/prov-templates/
kubectl -n ${NAMESPACE} create configmap -o yaml --dry-run=client  \
  swirrl-api-prov-templates $(ls *.trig | xargs printf -- ' --from-file=%s')  | \
  kubectl --namespace ${NAMESPACE} apply -f -
cd -

cd ../swirrl-workflow/prov-templates/
kubectl -n ${NAMESPACE} create configmap -o yaml --dry-run=client  \
  swirrl-workflow-prov-templates $(ls *.trig | xargs printf -- ' --from-file=%s')  | \
  kubectl --namespace ${NAMESPACE} apply -f -
cd -
