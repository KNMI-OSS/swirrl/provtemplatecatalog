#!/bin/bash

dumpfile=$1

if [ -z ${dumpfile} ] ; then
  echo "Usage: restore-db.bash dumpfile"
  exit 1
fi

kubectl -n swirrl cp ${dumpfile} prov-template-catalog-mongodb-0:/tmp/TemplateData.dump
kubectl -n swirrl exec prov-template-catalog-mongodb-0 -- mongorestore --archive=/tmp/TemplateData.dump
