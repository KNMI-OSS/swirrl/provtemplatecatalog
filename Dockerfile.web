FROM node:12

WORKDIR /prov-template-catalog
RUN mkdir /prov-template-catalog/static
ADD static static
ADD templates .

RUN npm install -g webpack && npm install axios
RUN npm install && npm install --save vue
RUN npm run build

FROM nginx:stable-bullseye

RUN apt-get purge -y libldap-* libxml2 && apt-get autoremove -y
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/share/nginx/html/
RUN mkdir /usr/share/nginx/html/prov-template-catalog
COPY --from=0 /prov-template-catalog/static /usr/share/nginx/html/prov-template-catalog/static
COPY --from=0 /prov-template-catalog/dist /usr/share/nginx/html/prov-template-catalog/static/dist
COPY --from=0 /prov-template-catalog/index.html /usr/share/nginx/html/prov-template-catalog/index.html
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

CMD echo "Starting NGINX" && nginx -g 'daemon off;'
